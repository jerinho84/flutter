import 'package:flutter/material.dart';

void main(){
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  String title = "Project title";

  @override
  Widget build(BuildContext context) => MaterialApp(title: title, home: ScreenSplash() .. title = title);
}

class ScreenSplash extends StatefulWidget {

  String title;

  @override
  State<StatefulWidget> createState() => StateSplash() .. title = title;
}

class StateSplash extends State<ScreenSplash>{

  String title;

  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(centerTitle: true, title: Text(title)),
    body: SafeArea(child: Center(child : Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text("Welcome !", style: TextStyle(
          fontSize: 30, fontWeight: FontWeight.bold
        ), textAlign: TextAlign.center,)
      ]
    )))
  );
}
